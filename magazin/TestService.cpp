//#include "TestService.h"
//#include "Service.h"
//#include "Repository.h"
//#include <assert.h>
//#include <iostream>
//void TestService::testAll() {
//	this->testAdd();
//	this->testRemove();
//	this->testModifyNume();
//	this->testModifyPret();
//	this->testModifyTip();
//	this->testModifyProducator();
//	this->testGetItem();
//	this->testSortareNume();
//	this->testSortarePret();
//	this->testSortareNumeSiTip();
//	this->testFiltrarePretMaiMic();
//	this->testFiltrarePretMaiMare();
//	this->testFiltrareNume();
//	this->testFiltareProducator();
//	this->testGetAll();
//}
//
//void TestService::testRemove() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.remove("Capsuni", "Kaufland");
//	Repository<Item> repository = service.getRepository();
//	assert(repository.getSize() == 0);
//}
//
//void TestService::testModifyNume() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.modifyNume("Capsuni", "Kaufland", "Banane");
//	Item item = service.getRepository().getItem(0);
//	assert(item.getNume() == "Banane");
//}
//
//void TestService::testModifyTip() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.modifyTip("Capsuni", "Kaufland", "Legume");
//	Item item = service.getRepository().getItem(0);
//	assert(item.getTip() == "Legume");
//}
//
//void TestService::testModifyPret() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.modifyPret("Capsuni", "Kaufland", 211);
//	Item item = service.getRepository().getItem(0);
//	assert(item.getPret() == 211);
//}
//
//void TestService::testModifyProducator() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.modifyProducator("Capsuni", "Kaufland", "Profi");
//	Item item = service.getRepository().getItem(0);
//	assert(item.getProducator() == "Profi");
//}
//
//void TestService::testGetItem() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	Item item = service.getItem("Capsuni", "Kaufland");
//	assert(item.getItem() == "Capsuni,Fructe,232.000000,Kaufland");
//}
//
//void TestService::testFiltrarePretMaiMic() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Fructe", 231, "Kaufland");
//	std::vector<Item> vector = service.filtrarePretMaiMic(231.5);
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Banane" && item1.getTip() == "Fructe" && item1.getPret() == 231 && item1.getProducator() == "Kaufland");
//}
//
//void TestService::testFiltrarePretMaiMare() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Fructe", 231, "Kaufland");
//	std::vector<Item> vector = service.filtrarePretMaiMare(231.5);
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Capsuni" && item1.getTip() == "Fructe" && item1.getPret() == 232 && item1.getProducator() == "Kaufland");
//	assert(vector.size() == 1);
//}
//
//void TestService::testFiltrareNume() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Fructe", 231, "Kaufland");
//	std::vector<Item> vector = service.filtrareNume("Capsuni");
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Capsuni" && item1.getTip() == "Fructe" && item1.getPret() == 232 && item1.getProducator() == "Kaufland");
//	assert(vector.size() == 1);
//}
//
//void TestService::testFiltareProducator() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Fructe", 231, "Profi");
//	std::vector<Item> vector = service.filtrareProducator("Kaufland");
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Capsuni" && item1.getTip() == "Fructe" && item1.getPret() == 232 && item1.getProducator() == "Kaufland");
//	assert(vector.size() == 1);
//}
//
//void TestService::testSortareNume() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Fructe", 231, "Kaufland");
//	std::vector<Item> vector = service.sortareNume();
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Banane" && item1.getTip() == "Fructe" && item1.getPret() == 231 && item1.getProducator() == "Kaufland");
//	Item item2 = vector[1];
//	assert(item2.getNume() == "Capsuni" && item2.getTip() == "Fructe" && item2.getPret() == 232 && item2.getProducator() == "Kaufland");
//}
//
//void TestService::testSortarePret() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Banane", "Asd", 231, "Kaufland");
//	std::vector<Item> vector = service.sortarePret();
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Banane" && item1.getTip() == "Asd" && item1.getPret() == 231 && item1.getProducator() == "Kaufland");
//	Item item2 = vector[1];
//	assert(item2.getNume() == "Capsuni" && item2.getTip() == "Fructe" && item2.getPret() == 232 && item2.getProducator() == "Kaufland");
//}
//
//void TestService::testSortareNumeSiTip() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	service.add("Capsuni", "Asd", 231, "Asd");
//	std::vector<Item> vector = service.sortareNumeSiTip();
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Capsuni" && item1.getTip() == "Asd" && item1.getPret() == 231 && item1.getProducator() == "Asd");
//	Item item2 = vector[1];
//	assert(item2.getNume() == "Capsuni" && item2.getTip() == "Fructe" && item2.getPret() == 232 && item2.getProducator() == "Kaufland");
//}
//
//void TestService::testGetAll() {
//	Service service;
//	service.add("Capsuni", "Asd", 231, "Asd");
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	std::vector<Item> vector = service.getAll();
//	Item item1 = vector[0];
//	assert(item1.getNume() == "Capsuni" && item1.getTip() == "Asd" && item1.getPret() == 231 && item1.getProducator() == "Asd");
//	Item item2 = vector[1];
//	assert(item2.getNume() == "Capsuni" && item2.getTip() == "Fructe" && item2.getPret() == 232 && item2.getProducator() == "Kaufland"); 
//}
//
//void TestService::testAdd() {
//	Service service;
//	service.add("Capsuni", "Fructe", 232, "Kaufland");
//	Repository<Item> repository = service.getRepository();
//	Item item = repository.getItem(0);
//	Item item2("Capsuni", "Fructe", 232, "Kaufland");
//	assert(item == item2);
//}
