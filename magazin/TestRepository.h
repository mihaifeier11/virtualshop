#pragma once
class TestRepository {
public:
	TestRepository();
	~TestRepository();

	void testAll();
	void testAdd();
	void testGetItem();
	void testRemove();
	void testGetSize();
	void testFind();

	void testUpdate();
};

