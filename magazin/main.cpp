#include <iostream>
#include "TestItem.h"
#include "TestRepository.h"
#include "TestService.h"
#include "Ui.h"
#include "FileRepository.h"
int main() {
	TestItem testItem;
	testItem.testAll();
	TestRepository testRepository;
	testRepository.testAll();
	//TestService testService;
	//testService.testAll();
	Ui ui;
	
	ui.run();
	
	return 0;
}