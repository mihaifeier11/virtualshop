#pragma once
#include <string>
class Item {
private:
	std::string nume;
	std::string tip;
	double pret;
	std::string producator;
public:
	Item() = default;
	Item(std::string nume, std::string tip, double pret, std::string producator);
	~Item() = default;

	std::string getNume() const;
	std::string getTip() const;
	double getPret() const;
	std::string getProducator() const;

	void modifyNume(const std::string newNume);
	void modifyTip(const std::string newTip);
	void modifyPret(const double newPret);
	void modifyProducator(const std::string newProducator);

	std::string getItem() const;

	bool operator==(const Item &item2);

	void operator=(const Item &item2);

	friend std::istream & operator>>(std::istream & is, Item & item);
	friend std::ostream & operator<<(std::ostream & is, Item & item);
};



