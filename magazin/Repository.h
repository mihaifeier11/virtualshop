#pragma once
#include <vector>

template<class T>

class Repository {
private:
	std::vector<T> repository;
public:
	Repository() = default;
	~Repository() = default;

	void add(const T item) {
		this->repository.push_back(item);
	}
	const T getItem(const int position) const {
		return this->repository[position];
	}
	void remove(const int position) {
		this->repository.erase(repository.begin() + position);
	}
	const int getSize() const{
		return this->repository.size();
	}
	int find(const T item) {
		int exist = -1;
		for (int i = 0; i < (int)repository.size(); i++) {
			if (this->repository[i] == item)
				exist = i;
		}
		return exist;
	}

	void update(const T item, const int position) {
		this->repository[position] = item;
	}

	std::vector<T> getRepository() {
		return repository;
	}
};



