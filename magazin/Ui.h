#pragma once
#include "Service.h"
#include "CosCumparaturi.h"



class Ui {
private:
	Service service;
	CosCumparaturi cosCumparaturi;


public:
	Ui() = default;
	~Ui() = default;
	void run();
	void printMenu();
	void adaugareProdus();
	void stergereProdus();
	void modificareNume();
	void modificareTip();
	void modificarePret();
	void modificareProducator();
	void afisareProduse();
	void cautareProdus();
	void filtrarePretMaiMic();
	void filtrarePretMaiMare();
	void filtrareNume();
	void filtrareProducator();
	void sortareNume();
	void sortarePret();
	void sortareNumeSiTip();
	void adaugareRandom();
	void undo();
	void adaugareProdusCos();
	void golireCos();
	void adaugareRandomCos();
};

