#pragma once
#include "FileRepository.h"
#include "Item.h"
class UndoAction
{
public:
	virtual void doUndo() = 0;
	virtual ~UndoAction() = default;
};

class AddUndo : public UndoAction
{
private:
	FileRepository<Item> &repo;
	Item item;

public:
	AddUndo(FileRepository<Item> &repo, const Item &b) : repo{ repo }, item{ b } {}

	void doUndo() override
	{
		int position = repo.find(item);
		repo.remove(position);
	}
};

class RemoveUndo : public UndoAction
{
	FileRepository<Item> &repo;
	Item b;

public:
	RemoveUndo(FileRepository<Item> &repo, const Item &b) : repo{ repo }, b{ b } {}
	void doUndo() override
	{
		repo.add(b);
	}
};

class UpdateUndo : public UndoAction
{
	FileRepository<Item> &repo;
	Item b;

public:
	UpdateUndo(FileRepository<Item> &repo, const Item &b) : repo{ repo }, b{ b } {}
	void doUndo() override
	{
		int position = repo.find(b);
		repo.update(b, position);
	}
};

class UndoException
{
	std::string message;

public:
	UndoException(std::string m) : message{ m } {}
	friend std::ostream &operator<<(std::ostream &out, const UndoException &ex)
	{
		out << ex.message;
		return out;
	}
};
